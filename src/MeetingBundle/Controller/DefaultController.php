<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Entity\Meeting;
use MeetingBundle\Form\MeetingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/main", name="index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $meetings = $em->getRepository('MeetingBundle:Meeting')->findAll();
        return $this->render('MeetingBundle:Meeting:show.html.twig', array('meetings' => $meetings));
    }
    /**
     * @Route("/meeting/show/{id}", name="meeting_detail")
     */
    public function detailShowAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $subscribe = false;
        $meeting = $em->getRepository('MeetingBundle:Meeting')->findOneById($id);


        $user = $em->getRepository('MeetingBundle:User')->findOneById($this->getUser());

        if ($meeting->getMembers()->contains($user)){
            $subscribe = true;
        }
        return $this->render('MeetingBundle:Meeting:detailShow.html.twig',
            array(
                'meeting' => $meeting,
                'subscribe' => $subscribe
            ));
    }
    /**
     * @Route("/meeting/{id}/subscribe", name="subscribe_meeting")
     */
    public function goMeetingAction(Request $request, $id){

        $user = $this->getUser();


        if ($user)
        {
            $em = $this->getDoctrine()->getManager();
            $meeting = $em->getRepository('MeetingBundle:Meeting')->findOneById($id);
            $user = $em->getRepository('MeetingBundle:User')->findOneById($user->getId());

            $members = $meeting->getMembers();
            if (!$members->contains($user)){
                $meeting->addMember($user);
            }
            $em->flush();
            $this->addFlash("success", "Вы успешно присоединились к встрече!!!");

            return $this->redirect($this->generateUrl('meeting_detail', array('id' => $meeting->getId())));
        } else {
            return $this->redirect($this->redirectToRoute('login'));

        }
    }

}


