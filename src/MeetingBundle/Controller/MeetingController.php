<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Entity\Meeting;
use MeetingBundle\Form\MeetingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class MeetingController extends Controller
{
    /**
     * @Route("/meetings", name="admin_meetings")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $meetings = $em->getRepository('MeetingBundle:Meeting')->findAll();
        return $this->render('MeetingBundle:Meeting:index.html.twig', array('meetings' => $meetings));
    }
    /**
     * @Route("/edit/meeting/{id}", name="admin_meeting_edit")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $meeting = $em->getRepository('MeetingBundle:Meeting')->findOneById($id);
        $form = $this->createForm(new MeetingType(), $meeting, array(
            'action' => $this->generateUrl('admin_meeting_edit', array('id' => $meeting->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-success')));
        if ($request->getMethod() === "POST"){
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();
                $this->addFlash("success", "Данные успешно обновлены!!!");
                return $this->redirect($this->generateUrl('admin_meeting_edit', array('id' => $meeting->getId())));
            }
        }

        return $this->render('MeetingBundle:Meeting:edit.html.twig', array(
            'meeting' => $meeting,
            'form'    => $form->createView()
        ));
    }
    /**
     * @Route("/new/meeting", name="admin_meeting_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $meeting = new Meeting();
        $form = $this->createForm(new MeetingType(), $meeting, array(
            'action' => $this->generateUrl('admin_meeting_new'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        if ($request->getMethod() === "POST"){
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($meeting);
                $em->flush();
                $this->addFlash("success", "Данные успешно сохранены!!!");
                return $this->redirect($this->generateUrl('admin_meeting_edit', array('id' => $meeting->getId())));
            }
        }

        return $this->render('MeetingBundle:Meeting:new.html.twig', array(
            'meeting' => $meeting,
            'form'    => $form->createView()
        ));
    }
     /**
     * @Route("/delete/meeting/{id}", name="admin_meeting_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $meeting = $em->getRepository('MeetingBundle:Meeting')->findOneById($id);
        $em->remove($meeting);
        $em->flush();
        return $this->redirect($this->generateUrl("admin_meetings"));
    }

}


