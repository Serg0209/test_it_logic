<?php

namespace MeetingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MeetingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => "Название встречи",
                'required' => false
            ))
            ->add('venue', 'text', array(
                'label' => "Место встречи"
            ))
            ->add('previewFile', 'file', array(
                'label' => "Миниатюра",
                'required' => false
            ))
            ->add('fullFile', 'file', array(
                'label' => "Изображение",
                'required' => false
            ))
            ->add('meetingDate', 'datetime', array(
                'label' => "Дата проведения",
                'date_widget' => 'single_text',
                'time_widget' => 'choice'
            ))
            ->add('description', 'textarea', array(
                'label' => "Описание"
            ))

            ->add('shortDescription', 'textarea', array(
                'label' => "Короткое описание"
            ))
            ->add('quantity', 'integer', array(
                'label' => "Количество учасников"
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MeetingBundle\Entity\Meeting'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'meeting';
    }
}
