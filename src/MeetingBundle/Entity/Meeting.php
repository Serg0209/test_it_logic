<?php
namespace MeetingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="MeetingBundle\Repository\MeetingRepository")
 * @ORM\Table(name="meetings")
 * @ORM\HasLifecycleCallbacks
 */
class Meeting
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @Assert\Length(
     *      min = 10,
     *      max = 50,
     *      minMessage = "Название встречи должно быть длинее 10 символов",
     *      maxMessage = "Название встречи должно быть не более 50 символов"
     * )
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $imgPreview;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $imgFull;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $meetingDate;

    /**
     * @ORM\Column(type="string")
     */
    protected $shortDescription;
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @ORM\Column(type="string")
     */
    protected $description;
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @ORM\Column(type="string")
     */
    protected $venue;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="meetings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="members",
     *      joinColumns={@ORM\JoinColumn(name="meeting_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     **/
    protected $members;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $previewFile;

    private $previewTemp;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $fullFile;

    private $fullTemp;

    public function __construct()
    {
        $this->members = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */

    public function getId()
    {
        return $this->id;
    }

    public function setPreviewFile(UploadedFile $file = null)
    {
        $this->previewFile = $file;
        // check if we have an old image path

        if (isset($this->imgPreview) && $this->imgPreview !== null) {
            // store the old name to delete after the update
            $this->previewTemp = $this->imgPreview;
            $this->imgPreview = null;
        } else {
            $this->imgPreview = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getPreviewFile()
    {
        return $this->previewFile;
    }
    public function setFullFile(UploadedFile $file = null)
    {
        $this->fullFile = $file;
        // check if we have an old image path

        if (isset($this->imgFull) && $this->imgFull !== null) {
            // store the old name to delete after the update
            $this->fullTemp = $this->imgFull;
            $this->imgFull = null;
        } else {
            $this->imgFull = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFullFile()
    {
        return $this->fullFile;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Meeting
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set imgPreview
     *
     * @param string $imgPreview
     * @return Meeting
     */
    public function setImgPreview($imgPreview)
    {
        $this->imgPreview = $imgPreview;

        return $this;
    }

    /**
     * Get imgPreview
     *
     * @return string 
     */
    public function getImgPreview()
    {
        return $this->imgPreview;
    }

    /**
     * Set imgFull
     *
     * @param string $imgFull
     * @return Meeting
     */
    public function setImgFull($imgFull)
    {
        $this->imgFull = $imgFull;

        return $this;
    }

    /**
     * Get imgFull
     *
     * @return string 
     */
    public function getImgFull()
    {
        return $this->imgFull;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Meeting
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set meetingDate
     *
     * @param \DateTime $meetingDate
     * @return Meeting
     */
    public function setMeetingDate($meetingDate)
    {
        $this->meetingDate = $meetingDate;

        return $this;
    }

    /**
     * Get meetingDate
     *
     * @return \DateTime 
     */
    public function getMeetingDate()
    {
        return $this->meetingDate;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Meeting
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Meeting
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set venue
     *
     * @param string $venue
     * @return Meeting
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;

        return $this;
    }

    /**
     * Get venue
     *
     * @return string 
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Meeting
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set user
     *
     * @param \MeetingBundle\Entity\User $user
     * @return Meeting
     */
    public function setUser(\MeetingBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MeetingBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add members
     *
     * @param \MeetingBundle\Entity\User $members
     * @return Meeting
     */
    public function addMember(\MeetingBundle\Entity\User $members)
    {
        $this->members[] = $members;

        return $this;

    }

    /**
     * Remove members
     *
     * @param \MeetingBundle\Entity\User $members
     */
    public function removeMember(\MeetingBundle\Entity\User $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMembers()
    {
        return $this->members;
    }
    public function getPreviewPath()
    {
        return null === $this->imgPreview
            ? null
            : $this->getUploadRootDir().'/thumbnail/'.$this->imgPreview;
    }
    public function getFullPath()
    {
        return null === $this->imgFull
            ? null
            : $this->getUploadRootDir().'/fullImages/'.$this->imgFull;
    }

    public function getPathPreviewImg()
    {
        return null === $this->imgPreview
            ? null
            : $this->getUploadDir().'/thumbnail/'.$this->imgPreview;
    }

    public function getPathFullImg()
    {
        return null === $this->imgFull
            ? null
            : $this->getUploadDir().'/fullImages/'.$this->imgFull;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/uploads';
    }
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    if (null !== $this->getPreviewFile()) {
            $this->getPreviewFile()->move($this->getUploadRootDir().'/thumbnail', $this->imgPreview);
            if (isset($this->previewTemp)) {
                unlink($this->getUploadRootDir().'/thumbnail/'.$this->previewTemp);
                $this->previewTemp = null;
            }
            $this->previewFile = null;
        }

     if (null !== $this->getFullFile()) {
            $this->getFullFile()->move($this->getUploadRootDir().'/fullImages', $this->imgFull);
            if (isset($this->fullTemp)) {
                unlink($this->getUploadRootDir().'/fullImages/'.$this->fullTemp);
                $this->fullTemp = null;
            }
            $this->fullFile = null;
        }

    }
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getPreviewFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imgPreview = $filename.'.'.$this->getPreviewFile()->guessExtension();
        }
        if (null !== $this->getFullFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imgFull = $filename.'.'.$this->getFullFile()->guessExtension();
        }
    }
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getPreviewPath();
        if ($file) {
            unlink($file);
        }
        $file = $this->getFullPath();
            if ($file) {
                unlink($file);
            }
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updated = new \DateTime();
    }
    public function getUpdated()
    {
        return $this->updated;
    }


}
